Teknikal Test Front End Developer Gamatechno 

Pertama-tama untuk project ini saya menggunakan javascript libray React JS dan saya menggunakan JSON-SERVER untuk Rest Api nya.
Walaupun task nya hanyalah membuat halaman login, tapi di project ini saya juga membuat halaman register, 
selain itu juga ketika kita berhasil login, kita akan diredirect ke halaman "/welcome".
Untuk ini saya juga sudah membuat routing page, jadi ketika kita belum login, tapi ingin mengakses langsung halaman "/welcome" akan di redirect ke halaman utama. 

Untuk dependensi yang saya gunakan adalah sebagai berikut :
"react"
"react-scripts"
"bootstrap"
"react-bootstrap"
"redux"
"react-redux"
"react-router"
"react-dom"
"react-router-dom"
"axios"



Untuk konfigurasi dan logic aplikasi, saya menggunakan redux untuk menyimpan data state management untuk login. 
Dan karena menggunakan RestApi, untuk menyimpan data login (session) saya menyimpannya di localstorage.

Untuk Konfigurasi RestAPI nya :
Port Json-Server adalah port : 4000
untuk data login awal adalah :
username : gama
password : 123

dan 

username : techno
password : 123