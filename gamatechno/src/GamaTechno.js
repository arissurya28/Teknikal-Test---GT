import React from 'react';
import LoginPage from './components/loginpage';
import RegisterPage from './components/registerpage';
import { Switch, Route } from 'react-router'
import Welcome from './components/welcome';


function App() {
  return (
    <div>
      <Switch>
        <Route path='/' component={LoginPage} exact/>
        <Route path='/register' component={RegisterPage} exact/>
        <Route path='/welcome' component={Welcome} exact/>
      
      </Switch>
      
    </div>
  );
}

export default App;
