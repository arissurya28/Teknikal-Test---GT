import React, { Component } from 'react';
import { Form,  Button } from 'react-bootstrap'
import './StyleComponent.css'
import { Link, Redirect } from 'react-router-dom'
import {connect} from 'react-redux'
import { LoginBerhasil } from './../redux/action'
import { url } from './../RestAPI/urlapi'
import Axios from 'axios'


class LoginPage extends Component {
    state = { 
        error:'',
        errorpassword:'',
        username:'',
        password:''
     }

    onLogin=()=>{
        
        var username=this.state.username
        var password=this.state.password
        if (username === "" || password === "" ) {
            this.setState({errorpassword:'Data Gaboleh Kosong'})
        } else {
            Axios.get(`${url}users?username=${username}&password=${password}`)
            .then(res=>{
                if(res.data.length){
                    localStorage.setItem('login',res.data[0].id)
                    this.props.LoginBerhasil(res.data[0])
                    
                }else{
                    this.setState({error:'Username atau Password Salah'})
                }
            }).catch((err)=>{
                console.log(err)
            })
        }
    }

    render() { 
        if(this.props.AuthLogin) {
            return <Redirect to={'/welcome'} />
        }

        return ( 
        <div className='loginpage'>
            <div className='loginstye'>
                
               <div className='container '>
                    <div className='row '>
                        <div className='col-md-12 col-xs-12 col-sm-12 loginform '>
                        <h4>Selamat Datang Di Aplikasi <br/> GamaTechno</h4>
                           <div className='formlogin' >
                           <Form>
                           <Form.Group controlId="formBasicEmail">
                               <Form.Label>Username</Form.Label>
                               <Form.Control type="email" placeholder="Enter Username" onChange={(e)=>this.setState({username:e.target.value})} />
                           </Form.Group>
                       
                           <Form.Group controlId="formBasicPassword">
                               <Form.Label>Password</Form.Label>
                               <Form.Control type="password" placeholder="Password" onChange={(e)=>this.setState({password:e.target.value})} />
                           </Form.Group>

                           <Form.Text>
                               <p>Belum Punya Akun? <Link to='/register'>Daftar disini.</Link></p>
                           </Form.Text>

                           {this.state.error===''?
                            null
                            :
                            <div className="alert alert-danger mt-2">
                                {this.state.error} <span onClick={()=>this.setState({error:''})} className='float-right'>close</span>
                            </div>
                    
                            }

                            {this.state.errorpassword===''?
                            null
                            :
                            <div className="alert alert-danger mt-2">
                                {this.state.errorpassword} <span onClick={()=>this.setState({errorpassword:''})} className='float-right'>close</span>
                            </div>
                    
                            }

                           <center> 
                               <Button variant="light" onClick={this.onLogin}  >
                               Masuk
                               </Button>
                           </center>
                            </Form>
                           </div>
                        </div>
                    </div>
               </div>
               
            </div>
        </div>
         );
    }
}
 
const MapstateToprops=(state)=>{
    return{
        AuthLogin:state.Auth.login
    }
  }
  
  
  export default connect(MapstateToprops,{LoginBerhasil}) (LoginPage);