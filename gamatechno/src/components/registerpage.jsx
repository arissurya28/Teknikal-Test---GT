import React, { Component } from 'react';
import { Form,  Button } from 'react-bootstrap'
import './StyleComponent.css'
import { Link, Redirect } from 'react-router-dom'
import { url } from './../RestAPI/urlapi'
import Axios from 'axios'




class RegisterPage extends Component {
    state = {
        redirectlogin:false,
        username:'',
        password:'',
        datagabolehkosong:'',
        suksesdaftar:'',
        usernamesudahada:''

      }

      onRegister = () => {
        console.log(this.state)
        var username=this.state.username; 
        var password=this.state.password;
        var newUser = { username, password };
        if (username === "" || password === "" ) {

          this.setState({datagabolehkosong:'Data Gaboleh Kosong'})

        } else {
          Axios.get(`${url}users?username=${username}`)
            .then(res1 => {
              if (res1.data.length === 0) {
                  Axios.post(`${url}users`, newUser)
                    .then(res => {

                        this.setState({suksesdaftar:'Berhasil Mendaftar, Silahkan Login'})
                        this.setState({redirectlogin: true });
                      
                    })
                    .catch(err1 => {
                      console.log(err1);
                    });
                
              } else {

                this.setState({usernamesudahada:'Username sudah ada, silahkan coba yang lain'})

              }
            })
            .catch(err => {
              console.log(err);
            });
        }
      }



    render() { 
        if (this.state.redirectlogin) {
            return <Redirect to={'/'} />
        }
        return ( 
        <div className='registerpage'>
            <div className='loginstye'>
                
               <div className='container '>
                    <div className='row '>
                        <div className='col-md-12 col-xs-12 col-sm-12 loginform '>
                        <h4>Selamat Datang Di Aplikasi <br/> GamaTechno</h4>
                           <div className='formlogin' >
                           <Form>
                           <Form.Group controlId="formBasicEmail">
                               <Form.Label>Username</Form.Label>
                               <Form.Control type="email" placeholder="Enter Username" onChange={(e)=>this.setState({username:e.target.value})} />
                           </Form.Group>
                       
                           <Form.Group controlId="formBasicPassword">
                               <Form.Label>Password</Form.Label>
                               <Form.Control type="password" placeholder="Password" onChange={(e)=>this.setState({password:e.target.value})} />
                           </Form.Group>

                           <Form.Text>
                               <p>Sudah Punya Akun? <Link to='/'>Masuk disini.</Link></p>
                           </Form.Text>
                           
                           {this.state.datagabolehkosong===''?
                           null
                           :
                           <div className="alert alert-danger mt-2">
                               {this.state.datagabolehkosong} <span onClick={()=>this.setState({datagabolehkosong:''})} className='float-right'>close</span>
                           </div>
                   
                           }

                           {this.state.suksesdaftar===''?
                           null
                           :
                           <div className="alert alert-danger mt-2">
                               {this.state.suksesdaftar} <span onClick={()=>this.setState({suksesdaftar:''})} className='float-right'>close</span>
                           </div>
                   
                           }

                           {this.state.usernamesudahada===''?
                           null
                           :
                           <div className="alert alert-danger mt-2">
                               {this.state.usernamesudahada} <span onClick={()=>this.setState({usernamesudahada:''})} className='float-right'>close</span>
                           </div>
                   
                           }





                           <center> 
                               <Button variant="light" onClick={this.onRegister} >
                               Daftar Akun
                               </Button>
                           </center>
                            </Form>
                           </div>
                        </div>
                    </div>
               </div>
               
            </div>
        </div>
         );
    }
}
 
export default RegisterPage;